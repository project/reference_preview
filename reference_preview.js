
/* Reference Preview Behavior
 * 
 * Using jQuery, add a new div below the input element,
 * then add a trigger to reference_preview when the
 * element changes
*/
Drupal.behaviors.reference_preview = function (context) {
  for (var field_id in Drupal.settings.reference_preview) {
    
    var content = "<div id='reference-preview-" + field_id + "' class='reference-preview' style='display:none'></div>";
    
    // Works for autocompelte node references
    $('input[id^=edit-'+ field_id +'-][id$=-nid-nid]').each(function() {
      if (!$(this).hasClass('reference-preview-processed')) {
        //@@TODO -- check length -> could match fields that are named similarliry
        
        // Add our div after the input-element, then add a trigger, then add a processed class 
        $(this)
          .after(content)
          .change(Drupal.reference_preview)
          .addClass('reference-preview-processed');
          
        // Attach the field ID to the input element for later
        jQuery.data(this, 'reference_preview_field_id', field_id);
      }
    });

  }
}

/* Reference Preview
 * 
 * Populate the reference-preview div below the input element
 * using AHAH.
*/
Drupal.reference_preview = function(event) {
  var $field = $(event.target);
  var val = $field.val();
  var field_id = jQuery.data(event.target, 'reference_preview_field_id');
  var $reference_preview = $field.parent().find('div.reference-preview');
  $reference_preview.html('');
  
  // Parse out "[nid: 123]" from the input element $field
  var regex = new RegExp("\[nid:[0-9]*\]");
  var m = regex.exec(val); // matches [nid: 123]
  
  // If we found a match, then get the nid, and then do the AHAH
  if (m !== null) {
    var nid = m[0].substring(5, m[0].length -1 );
    $reference_preview.load(Drupal.settings.basePath + 'reference_preview_ahah/'+nid+'/'+field_id).show();
  }
  
}